/*
Programa principal de la tarea 0.

Laboratorio de Programacion 3.
InCo-FI-UDELAR
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ABB.h"
#include "Cola.h"
#include "ColaPrioridad.h"
#include "Lista.h"
#include "Pila.h"


// Cantidad maxima de elementos en la Pila
#define COTA_PILA 10

// Cantidad maxima de elementos en la Cola de Prioridad
#define COTA_COLA_PRIO 10

// mensajes de salida
char msg_bienvenida []    = "Tarea Introductoria - 2015 - Programacion 3.";
char msg_agregar_pila []  = "se ha agregado en la Pila";
char msg_tope_pila []     = "se saco de la Pila.";
char msg_pila_vacia []    = "Pila vacia";
char msg_agregar_cola []  = "se ha agregado en la Cola.";
char msg_frente_cola []   = "se saco de la Cola.";
char msg_cola_vacia []    = "Cola vacia";
char msg_agregar_prio []  = "se ha agregado en la Cola de Prioridad.";
char msg_prio_vacia []    = "Cola de Prioridad vacia";
char msg_sacar_prio []    = "se saco de la Cola de Prioridad";
char msg_agregar_lista [] = "se ha insertado en la Lista.";
char msg_kesimo_lista []  = "es el elemento buscado de la Lista.";
char msg_no_haykesimo []  = "No hay suficientes elementos.";
char msg_agregar_abb []   = "se ha agregado en el ABB.";
char msg_arbol_vacio []   = "Arbol vacio.";
char msg_no_se_hizo []    = "No se hizo nada.";
char msg_final []         = "FIN";

// nombres de comandos
char cmnd_apilar []        = "Apilar";
char cmnd_desapilar []     = "Desapilar";
char cmnd_encolar []       = "Encolar";
char cmnd_desencolar []    = "Desencolar";
char cmnd_encolarPrio[]	   = "EncolarPrioridad";
char cmnd_desencolarPrio[] = "DesencolarPrioridad";
char cmnd_insertar []      = "InsertarLista";
char cmnd_kesimo []        = "KesimoLista";
char cmnd_agregar_abb []   = "AgregarAbb";
char cmnd_imprimir_abb []  = "ImprimirAbb";
char cmnd_salir []         = "salir";


void imprimir_rec (ABB abb, int h)
{
    if (abb != NULL)
    {
        imprimir_rec (arbolIzquierdo (abb), h + 1);
        for (int i = 0; i < h; i++) printf (" ");
        printf ("%d\n", valorABB (abb));
        imprimir_rec (arbolDerecho (abb), h + 1);
    }
}


int main()
{

    char buffer [80];// = new char[80];
    int valor;
    int prio;
    bool fin = false;


    Pila p;
    Cola c;
    ColaPrioridad cp;
    Lista l;
    ABB abb;

    crearPila (COTA_PILA, p);
    crearCola (c);
    crearColaPrioridad(cp, COTA_COLA_PRIO);
    crearLista (l);
    crearABB (abb);


    printf ("%s\n\n", msg_bienvenida);

    while (!fin)
    {
        printf (">");
        scanf ("%s", buffer);

        //PILA
        if (strcmp (buffer, cmnd_apilar) == 0)
        {
            scanf (" %d", &valor);
            if (esLlenaPila (p)) printf ("%s\n", msg_no_se_hizo);
            else
            {
                apilar (valor, p);
                printf ("%d %s\n", valor, msg_agregar_pila);
            }
        }
        else if (strcmp (buffer, cmnd_desapilar) == 0)
        {
            if (esVaciaPila (p)) printf ("%s\n", msg_pila_vacia);
            else
            {
                valor = cima (p);
                desapilar (p);
                printf ("%d %s\n", valor, msg_tope_pila);
            }
        }
        //COLA
        else if (strcmp (buffer, cmnd_encolar) == 0)
        {
            scanf (" %d", &valor);
            encolar (valor, c);
            printf ("%d %s\n", valor, msg_agregar_cola);
        }
        else if (strcmp (buffer, cmnd_desencolar) == 0)
        {
            if (esVaciaCola (c)) printf ("%s\n", msg_cola_vacia);
            else
            {
                valor = frente (c);
                desencolar (c);
                printf ("%d %s\n", valor, msg_frente_cola);
            }
        }
        //COLA PRIORIDAD
        else if (strcmp (buffer, cmnd_encolarPrio) == 0)
        {
          scanf (" %d %d", &valor, &prio);
	  if (esLlenaColaPrioridad (cp)) printf ("%s\n", msg_no_se_hizo);
	  else
	  {
	      encolarColaPrioridad (cp, valor, prio);
	      printf ("%d %s\n", valor, msg_agregar_prio);
	  }
        }
        else if (strcmp (buffer, cmnd_desencolarPrio) == 0)
        {
            if (esVaciaColaPrioridad (cp)) printf ("%s\n", msg_prio_vacia);
            else
            {
                valor = minimoColaPrioridad (cp);
		removerMinimoColaPrioridad (cp);
                printf ("%d %s\n", valor, msg_sacar_prio);
            }
        }
        //LISTA
        else if (strcmp (buffer, cmnd_insertar) == 0)
        {
            scanf (" %d", &valor);
            consLista (valor, l);
            printf ("%d %s\n", valor, msg_agregar_lista);
        }
        else if (strcmp (buffer, cmnd_kesimo) == 0)
        {
            int posicion;
            scanf (" %d", &posicion);
            Lista resto = l;
            while ((posicion > 1) && ! esVaciaLista (resto)) {
                posicion--;
                restoLista (resto);
            }
            if (! esVaciaLista (resto)) {
                valor = primeroLista (resto);
                printf ("%d %s\n", valor, msg_kesimo_lista);
            } else {
                printf ("%s\n", msg_no_haykesimo);
            }
        }
        //ABB
        else if (strcmp (buffer, cmnd_agregar_abb) == 0)
        {
            scanf (" %d", &valor);
            if (agregarABB (valor, abb)) {
                printf ("%d %s\n", valor, msg_agregar_abb);
            } else {
                printf ("%s\n", msg_no_se_hizo);
            }
        }
        else if (strcmp (buffer, cmnd_imprimir_abb) == 0)
        {
            if (esVacioABB (abb)) printf ("%s\n", msg_arbol_vacio);
            else
            {
                printf ("\n");
                imprimir_rec (abb, 0);
            }
        }
        // FIN
        else if (strcmp (buffer, cmnd_salir) == 0)
        {
            fin = true;
            printf ("%s\n", msg_final);
        }
        // ERROR
        else
        {
            printf ("Comando no reconocido \n");
            exit (1);
        }
    }
    destruirABB (abb);
    destruirCola (c);
    destruirColaPrioridad(cp);
    destruirLista (l);
    destruirPila (p);
    return 0;
}


/*
        else if (strcmp (buffer, cmnd_niveles) == 0)
        {
            if (esVacioABB (abb))
            {
                printf ("%s\n", msg_arbol_vacio);
            }
            else
            {
                Lista aux, l = listaPorNiveles (abb);
                aux = l;
                while (! esVaciaLista (aux))
                {
                    printf (" %d", primeroLista (aux));
                    restoLista (aux);
                }
                printf (".\n");
                destruirLista (l);
            }
        }
*/
