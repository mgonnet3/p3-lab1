#ifndef _ABB_H
#define _ABB_H

/*
Arbol binario de busqueda.

Laboratorio de Programacion 3.
InCo-FI-UDELAR
*/

#include "Lista.h"
#include "tipoT.h"

struct AuxABB;
typedef AuxABB* ABB;

void crearABB (ABB &abb);
/* Devuelve en abb el arbol vacio. */

bool agregarABB (tipoT t, ABB &abb);
/* Agrega el elemento t en abb y devuelve 'true' si t no esta en abb.
   En otro caso no hace nada y devuelve 'false'. */

bool esVacioABB (ABB abb);
/* Devuelve 'true' si abb es vacio, 'false' en otro caso. */

tipoT valorABB (ABB abb);
/* Devuelve el valor de la raiz de abb.
   Precondicion: ! esVacioABB(abb). */

ABB arbolIzquierdo (ABB abb);
/* Devuelve el subarbol izquierdo de abb.
   Precondicion: ! esVacioABB(abb). */

ABB arbolDerecho (ABB abb);
/* Devuelve el subarbol derecho de abb.
   Precondicion: ! esVacioABB(abb).*/

void destruirABB (ABB &abb);
/* libera toda la memoria ocupada por abb.*/

#endif /* _ABB_H */
