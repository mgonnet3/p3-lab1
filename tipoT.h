#ifndef _TIPO_T
#define _TIPO_T

typedef unsigned int tipoT;

bool esMayor (tipoT t1, tipoT t2);

bool esMenor (tipoT t1, tipoT t2);

bool esIgual (tipoT t1, tipoT t2);

#endif /* _TIPO_T */