#ifndef _PILA_H
#define _PILA_H

/*
Pila acotada.

Laboratorio de Programacion 3.
InCo-FI-UDELAR
*/

#include "tipoT.h"

struct AuxPila;
typedef AuxPila* Pila;

void crearPila (int cota, Pila &p);
/* Devuelve en p la pila vacia, que podra contener hasta cota elementos.*/

bool esVaciaPila (Pila p);
/* Devuelve 'true' si p es vacia, 'false' en otro caso.*/

bool esLlenaPila (Pila p);
/* Devuelve 'true' si p tiene cota elementos, donde cota es el valor del
   parametro pasado en crearPila, 'false' en otro caso.*/

void apilar (tipoT t, Pila &p);
/* Si !esLlenaPila (p) inserta t en la cima de p,
   en otro caso no hace nada.*/

tipoT cima (Pila p);
/* Devuelve la cima de p.
   Precondicion: ! esVaciaPila(p).*/

void desapilar (Pila &p);
/* Remueve la cima de p.
   Precondicion: ! esVaciaPila(p).*/

void destruirPila (Pila &p);
/* libera toda la memoria ocupada por p.*/

#endif /* _PILA_H */
