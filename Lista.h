#ifndef _LISTA_H
#define _LISTA_H

/*
Lista.

Laboratorio de Programacion 3.
InCo-FI-UDELAR
*/

#include "tipoT.h"

struct AuxLista;
typedef AuxLista* Lista;

void crearLista (Lista &l);
/* Devuelve en l la lista vacia.*/

void consLista (tipoT t, Lista &l);
/* Construye la lista l, con t al principio de la actual l.*/

bool esVaciaLista (Lista l);
/* Devuelve 'true' si l es vacia, 'false' en otro caso.*/

tipoT primeroLista (Lista l);
/* Devuelve el valor del primer elemento de l.
    Precondicion: !esVaciaLista (l).*/

void restoLista (Lista & l);
/* Cambia l a su resto.
   Precondicion: !esVaciaLista (l).*/

void destruirLista (Lista &l);
/* Libera toda la memoria ocupada por l.*/

#endif /* _LISTA_H */
