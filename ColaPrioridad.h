#ifndef _COLAPRIORIDAD_H
#define _COLAPRIORIDAD_H

/*
Cola de prioridad acotada de elementos de tipoT.

Laboratorio de Programacion 3.
InCo-FI-UDELAR
*/

#include "tipoT.h"

struct AuxColaPrioridad;
typedef AuxColaPrioridad * ColaPrioridad;

void crearColaPrioridad (ColaPrioridad &cp, int tamanio);
/*	Devuelve en 'cp' una cola de prioridad vacía,
	que podrá contener hasta 'tamanio' elementos.
	Precondición: tamanio > 0. */

void encolarColaPrioridad (ColaPrioridad &cp, tipoT t, int prio);
/*	Agrega a 'cp' el elemento 't' con prioridad 'prio'
 	Precondición: ! esLlenaColaPrioridad (cp). */

bool esVaciaColaPrioridad (ColaPrioridad cp);
/*	Devuelve 'true' si 'cp' es vacía, 'false' en otro caso. */

bool esLlenaColaPrioridad (ColaPrioridad cp);
/*	Devuelve 'true' si 'cp' está llena, 'false' en otro caso. */

tipoT minimoColaPrioridad (ColaPrioridad cp);
/*	Devuelve el elemento de 'cp' que tiene asignada menor prioridad
	(si más de uno cumple esa condición devuelve cualquiera de ellos).
	Precondición: ! esVaciaColaPrioridad (cp). */

void removerMinimoColaPrioridad (ColaPrioridad &cp);
/*	Remueve de 'cp' el elemento de menor prioridad
	(si más de uno cumple esa condición remueve cualquiera de ellos).
	Precondición: ! esVaciaColaPrioridad (cp). */

void destruirColaPrioridad (ColaPrioridad &cp);
/*	Libera toda la memoria ocupada por 'cp'. */

#endif /* _COLAPRIORIDAD_H */
