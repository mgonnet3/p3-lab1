#ifndef _COLA_H
#define _COLA_H

/*
Cola.

Laboratorio de Programacion 3.
InCo-FI-UDELAR
*/

#include "tipoT.h"

struct AuxCola;
typedef AuxCola* Cola;

void crearCola (Cola & c);
/* Devuelve en c la cola vacia.*/

bool esVaciaCola (Cola c);
/* Devuelve 'true' si c es vacia, 'false' en otro caso.*/

void encolar (tipoT t, Cola &c);
/* Agrega el elemento t al final de c.*/

tipoT frente (Cola c);
/* Devuelve el primer elemento de c
   Precondicion: ! esVaciaCola(c).*/

void desencolar (Cola &c);
/* Remueve el primer elemento de c.
   Precondicion: ! esVaciaCola(c).*/

void destruirCola (Cola &c);
/* Libera toda la memoria ocupada por c.*/

#endif /* _COLA_H */
